# genesis-tool

Genesis tool for Blacknet.
Based on [burnt-explorer](https://gitlab.com/KrzysiekJ/burnt-explorer)
Usage:

```
genesis-tool <start height> <stop height>
```

It is assumed that you have `blackcoind` binary available in `$PATH`. If you want to use another command, set the `BLACKCOIND_COMMAND` environment variable.

Output format is [CSV (RFC 4180)](https://tools.ietf.org/html/rfc4180) without header, with minimal double quotes usage and with the following columns:

```
<block height>,<block time>,<transaction id>,<vout index>,<public key>,<satoshis burnt>
```

[`bitcoin-script` package](https://hackage.haskell.org/package/bitcoin-script) is used for decoding scripts.

## License

This software is licensed under under [the Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the “License”); you may not use this software except in compliance with the License. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
